﻿using RIM.Entities.Concrete;
using RIM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RIM.WebUI.Controllers
{
    public class RIMController : Controller
    {
        #region [PROPERTIES]

        private IRover _IRover;

        #endregion

        #region [CONSTRUCTORS]

        public RIMController(IRover IRover)
        {
            _IRover = IRover;
        }

        #endregion

        #region [CONTROLLER ACTIONS]
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ProcessRover(RoverEntity RoverEntity)
        {
            return Json(_IRover.ProcessRover(RoverEntity));
        }

        #endregion
    }
}