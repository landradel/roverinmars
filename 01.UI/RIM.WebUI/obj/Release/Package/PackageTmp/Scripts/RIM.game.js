﻿/**
 * @license LuisRoverInMarsJS v0.0.1
 */

var roverPiece;

function StartRover(width, height) {
    roverPiece = new RoverComponent(20, 20, "blue", 20, 20);
    roverLand.start(width, height);
}

var roverLand = {
    canvas : document.createElement("canvas"),
    start: function (width, height) {
        this.canvas.width = width;
        this.canvas.height = height;
        this.context = this.canvas.getContext("2d");
        
        var container = document.getElementById("game-container");
        if (container) {
            container.appendChild(this.canvas, document.body.childNodes[0]);
        }
        else {
            document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        }
        //this.interval = setInterval(updateGameArea, 20);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function RoverComponent(width, height, color, x, y) {
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;    
    this.update = function(){
        ctx = roverLand.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}

window.UpdateRoverLocation = function (x, y) {
    console.log(x, y);
    roverLand.clear();
    roverPiece.x = x;
    roverPiece.y = y;
    roverPiece.update();
}

function HideExplanation() {
    $(".explanation-container").toggleClass("hide");
}