﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;
using RIM.Business;
using RIM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

//[assembly: OwinStartupAttribute(typeof(RIM.WebUI.Startup))]
[assembly: OwinStartup(typeof(RIM.WebUI.Startup))]

namespace RIM.WebUI
{
    public class Startup
    {
        /// <summary>
        ///     Proveedor
        /// </summary>
        public IServiceProvider Services { get; set; }

        /// <summary>
        ///     Metodo principal de la clase
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            // Para obtener más información acerca de cómo configurar su aplicación, visite http://go.microsoft.com/fwlink/?LinkID=316888
            //Se invoca el metodo ConfigureServices para agregar las dependencias
            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);
        }

        /// <summary>
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //Se agregan las dependencias interfaz,clase
            services.AddSingleton<IRover, RolverBL>();

            //Se agregan los controles a los servicios 
            services.AddControllersAsServices(typeof(Startup).Assembly.GetExportedTypes()
          .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
          .Where(t => typeof(IController).IsAssignableFrom(t)
                      || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));
        }
    }

    /// <summary>
    ///     Implementación de IDependencyResolver
    /// </summary>
    public class DefaultDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// </summary>
        protected IServiceProvider serviceProvider;

        /// <summary>
        /// </summary>
        /// <param name="serviceProvider"></param>
        public DefaultDependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            return serviceProvider.GetService(serviceType);
        }

        /// <summary>
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return serviceProvider.GetServices(serviceType);
        }
    }

    /// <summary>
    /// </summary>
    public static class ServiceProviderExtensions
    {
        /// <summary>
        /// </summary>
        /// <param name="services"></param>
        /// <param name="controllerTypes"></param>
        /// <returns></returns>
        public static IServiceCollection AddControllersAsServices(this IServiceCollection services,
            IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
                services.AddTransient(type);

            return services;
        }
    }
}