﻿/**
 * @license LuisRoverInMarsJS v0.0.1
 */
// Get app module
var app = angular.module('RIMApp', []);

// Service
app.service('roverService', ['$http', function ($http) {

    // expose a saveRecipe function from your service
    // that takes a recipe object
    this.processRover = function (rover) {
        // return a Promise object so that the caller can handle success/failure
        return $http({ method: 'POST', url: '/RIM/ProcessRover', data: rover });
    }

}]);

// Controller
app.controller('RIMBasicController', ['$scope', 'roverService',
    function ($scope, roverService) {
        
        // Initialize model
        $scope.Rover = {
            Command: "",
            StageHeight: "",
            StageWidth: "",
            XCoordinate: "",
            YCoordinate: "",
            ActualDirection: 0,
            ActualDirectionString: "North"
        };

        // declare a controller function that delegates to your service to save the processRover
        $scope.processRover = function (rover) {
            
            // call the service, handle then/error from within your controller
            roverService.processRover(rover).then($scope.processServiceThen, $scope.processServiceError);
        }

        // Then implementation
        $scope.processServiceThen = function (response) {
            // Get the data
            var data = response.data;

            // Validate result
            if (data.Result) {
                debugger
                // Update coordinate info
                $scope.Rover.XCoordinate = data.XCoordinate;
                $scope.Rover.YCoordinate = data.YCoordinate;
                $scope.Rover.ActualDirectionString = data.ActualDirectionString;
                $scope.Rover.ActualDirection = data.ActualDirection;
                
                var intervalFunctionID;

                // Iterate coordinates
                for (var i = 0; i < data.Coordinates.length; i++) {
                    
                    // Update coordinate
                    window.UpdateRoverLocation(data.Coordinates[i].XCoordinate, data.Coordinates[i].YCoordinate);

                    
                }
            }
            else {
                // Show message
                bootbox.alert(data.ResultTypeString);
            }
            
        }

        // Error implementation
        $scope.processServiceError = function (response) {
            // Show error message
            bootbox.alert("Sorry! The command couldn't be processed.");
        }

        // Set land dimensions
        $scope.SetLandDimensions = function () {
            // Get the value of the inputs
            var landWidthStr = $scope.Rover.LandWidthPercentage;
            var landHeightStr = $scope.Rover.LandHeightPercentage;

            // Get window width less the 22 percent of the width
            var fullWindowWidth = $("#simulation-panel").width() - $("#simulation-panel").width() * 0.22;
            var fullWindowHeight = window.innerHeight - window.innerHeight * 0.22;

            // Land width int value
            var landWidthInt = parseInt(landWidthStr);
            var landHeightInt = parseInt(landHeightStr);
            var landWidthPixelIntValue = (landWidthInt * fullWindowWidth) / 80;
            var landHeightPixelIntValue = (landHeightInt * fullWindowHeight) / 80;

            // Init canvas
            StartRover(landWidthPixelIntValue, landHeightPixelIntValue);

            // Set initial point
            window.UpdateRoverLocation(0, landHeightPixelIntValue - 20);

            // Set global land values
            window.landMaxWidth = landWidthPixelIntValue - 20;
            window.landMaxHeight = landHeightPixelIntValue - 20;

            window.landMaxWidth = parseInt(window.landMaxWidth);
            window.landMaxHeight = parseInt(window.landMaxHeight);

            // Set initial values
            $scope.Rover.StageWidth = window.landMaxWidth;
            $scope.Rover.StageHeight = landMaxHeight;
            $scope.Rover.XCoordinate = 0;
            $scope.Rover.YCoordinate = window.landMaxHeight - 20;
        }

        // Send command implementation
        $scope.SendCommand = function () {
            // Call the service
            $scope.processRover($scope.Rover);
        }



    }]);