﻿using RIM.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Interfaces
{
    public interface IRover
    {
        /// <summary>
        /// Get the Rover Result entity
        /// </summary>
        /// <param name="Rover">Rover entity to process</param>
        /// <returns></returns>
        RoverResultEntity ProcessRover(RoverEntity Rover);
    }
}
