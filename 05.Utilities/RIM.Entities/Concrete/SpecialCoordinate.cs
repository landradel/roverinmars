﻿using RIM.Entities.Abstract;

namespace RIM.Entities.Concrete
{
    public class SpecialCoordinate : Coordinate
    {
        #region [PROPERTIES]

        private int index;
        public int Index
        {
            get
            {
                return index;
            }

            set
            {
                index = value;
            }
        }

        #endregion

        #region [CONSTRUCTORS]

        /// <summary>
        /// Empty constructor
        /// </summary>
        public SpecialCoordinate()
        {

        }

        /// <summary>
        /// Set the values of the RoverResult entity
        /// </summary>
        /// <param name="Index">Index of the object</param>
        /// <param name="XCoordinate">XCoordinate value</param>
        /// <param name="YCoordinate">YCoordinate value</param>
        public SpecialCoordinate(int Index, int XCoordinate, int YCoordinate) : base(XCoordinate, YCoordinate)
        {
            index = Index;
        }

        #endregion

    }
}