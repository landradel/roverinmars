﻿using RIM.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Entities.Concrete
{
    public enum DirectionEnum
    {
        [StringValue("North")]
        North = 0,

        [StringValue("South")]
        South = 1,

        [StringValue("East")]
        East = 3,

        [StringValue("West")]
        West = 4
    }
}
